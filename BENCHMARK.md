# Benchmark

**Recently Updated**: 2016-06-06

## 0. Symposium

**License**: MIT

**Techstack**
- PHP
- MySQL
- Git


## 1. discourse

open source

**License**: GPL version 2

**Techstack**
- Ruby on Rails
- VirtualBox
- Vagrant
- Git

**Features**
- Support markdown style when create a new topic(default).
- Support to send messages to users.
- Support preview what you write a topic or a message.
- No page navigation link.

**Advantage**
- A lot of features (All Free, but host is need to pay)
- Support on PC, Mobile and Tablet.
- Simple and easy to use.
- Translations are available for 17 languages and counting.
- There is plugin system to extend.
- Provides demo in official website.
- Provides Detailed tutorials.

**Disadvantage**
- Expensive hosting
- Complicated setup if you've never used Ruby on Rails before


## 2. phpBB

free and open source.

**Recently updated**: 2016-04-17 v3.1.9

**License**: GPL version 2

**Techstack**
- PHP
- JIRA
- Composer

**Advantage**
- Provides demo in official website.

**Disadvantage**
- Not modern style (Such as font, color, icon and etc)
- No provides sharing to social network services.


## 3. myBB

free and open source, community-based forum software project.

**Recently updated**: 2016-03-11 v1.8.7

**License**: LGPL version 3

**Advantage**
- There is plugin system to extend.

**Disadvantage**
- No provides demo in official website.
- Dated design

